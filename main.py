# coding:utf8

import json
import nltk
import numpy as np
import os

from BD import BD
from matrice import WordMatrix
from Clustering import WMCluster


#------- MENU -------#

#--- Récupération du dictionnaire stocké dans dico.txt ---#

directory = '../extensions'
dico = {}
fichier = open("BD/dico.txt","r")
fichierdico = fichier.read()
dico = eval(fichierdico)
fichier.close()
#- Variables utilisées dans le Menu -#

x = 0
y = 0

while x != 1 :

    #- Initialisation du Menu -#
    
    print("\nMENU \n\n 1. Chercher une carte similaire \n 2. Créer un nouveau dictionnaire \n 3. Ajouter une extension au dictionnaire existant \n 4. Supprimer une extension au dictionnaire existant \n 5. Quitter \n")
    menu = input()

    #- Menu de recherche de cartes similaires -#
    if menu == "1" :
        carte=input("\nSaisissez la carte dont vous cherchez des équivalents et qui est présente dans les extensions ou faites 'Fin' pour revenir au Menu. \n")
        present = False
        if(carte!="Fin"):
            for i in dico:
                if i == carte:
                    present = True
            while(present==False):
                carte=input("\nCette carte n'est pas présente, choisissez une carte présente dans les extensions. \n")
                for i in dico:
                    if i == carte:
                        present = True
            budget = int(input("\nChoisissez un budget sachant que plus la valeur est grande, plus cela prendra du temps. \n"))
            k = int(input("\nCombien de centres voulez-vous au maximum (minimum 6)? \n"))
            while(k<3):
                 k = int(input("Il ne peux pas y avoir moins de 6 centres"))
        #matrice_test = {'Azerty':{'Bzerty':0.23,'Czerty':0.22,'Dzerty':0.90},'Bzerty':{'Azerty':0.23,'Czerty':0.96,'Dzerty':0.967},'Czerty':{'Azerty':0.22,'Bzerty':0.96,'Dzerty':0.267},'Dzerty':{'Azerty':0.90,'Bzerty':0.967,'Czerty':0.267}}
            test = WordMatrix.WMatrix(dico)
        
            cluster = WMCluster.max_silhouette(test,budget,k)
            for i in cluster[1]:#Pour tout les clusters pour le meilleur score silhouette
                for card in cluster[1][i]:#Pour toutes les cartes de chaque clusters
                    if carte==card:#Si la carte chercher par l'utilisateur est trouvée
                        if i == len(cluster[1])-1:#Si c'est le dernier clusters
                            print(cluster[1])
                            print("Aucune carte proche n'a été trouvée, n'hésitez à relancer le programme avec d'autres parametres ou ajouter d'autres extensions. \n")
                        else:
                            print("Elle est présente dans le cluster \n",cluster[1][i])

    #- Menu de création du dictionnaire -#
    
    if menu == "2" :
        print("\nLes extensions disponibles sont : \n")
        tabextension = []
        for filename in os.listdir(directory) :
            filename = filename.replace(".json","")
            tabextension.append(filename)
            print(" ",filename)
        print("\nSelectionnez l'extension que vous voulez utiliser en écrivant son abréviation ou ecrivez 'Fin' pour revenir au Menu : \n")
        while x != 1 :
            ext1 = input()
            if ext1 in tabextension :
                extdico = '../extensions/'+ext1+'.json'
                BD.create_dico('../extensions/'+ext1+'.json',dico)
                fichier = open("BD/dico.txt","r")
                fichierdico = fichier.read()
                dico = eval(fichierdico)
                fichier.close()
                ext1 = str(ext1)
                tabextension.remove(ext1)
                print("\nLe dico a bien été créé.\nPour ajouter d'autres extensions ecrivez son abréviation.\n\nLorsque vous aurez terminé, écrivez 'Fin' pour revenir au Menu.\n")
                x = 1
            elif ext1 == "Fin" :
                x = 1
                y = 1
            else :
                print("\nL'extension donnée n'est pas disponible, veuillez réessayer ou ajouter l'extension dans 'BagOfMagic/extensions' après l'avoir télécharger sur 'https://mtgjson.com/downloads/all-sets/'.\n")
        x = 0
        while x != 1 and y != 1:
            choixextension = input()
            if choixextension in tabextension :
                newextdico = '../extensions/'+choixextension+'.json'
                BD.addset(newextdico,dico)
                fichier = open("BD/dico.txt","r")
                fichierdico = fichier.read()
                dico = eval(fichierdico)
                fichier.close()
                tabextension.remove(choixextension)
                print("\nLa nouvelle extension a bien été ajoutée.\nAjoutez une autre extension grâce a son abréviation ou ecrivez 'Fin' pour revenir au Menu.\n")
            elif choixextension == "Fin" :
                x = 1
            else :
                print("\nL'extension donnée n'est pas disponible, veuillez réessayer ou ajouter l'extension dans 'BagOfMagic/extensions' après l'avoir téléchargé sur 'https://mtgjson.com/downloads/all-sets/'.\n")
        x = 0
        y = 0

    #- Menu d'ajout d'extension au dictionnaire -#
        
    if menu == "3" :
        print("\nLe dico contient les extensions suivantes :\n")
        tabcode = []
        for carte in list(dico):
            if dico[carte]["Extension"] not in tabcode :
                tabcode.append(str(dico[carte]["Extension"]))
        for i in tabcode :
            print(i)
        print("\nLes extensions disponibles sont :\n")
        tabextension = []
        for filename in os.listdir(directory) :
            filename = filename.replace(".json","")
            tabextension.append(filename)
        addtabextension = set(tabextension) - set(tabcode)
        for j in addtabextension :
            print(j)
        print("\nSelectionnez l'extension (en notant son abréviation) que vous voulez ajouter au dico ou ecrivez 'Fin' pour revenir au Menu.\n")
        while x != 1 :
            choixextension = input()
            if choixextension in addtabextension :
                newextdico = '../extensions/'+choixextension+'.json'
                BD.addset(newextdico,dico)
                fichier = open("BD/dico.txt","r")
                fichierdico = fichier.read()
                dico = eval(fichierdico)
                fichier.close()
                addtabextension.remove(choixextension)
                print("\nLa nouvelle extension a bien été ajoutée.\nAjoutez une autre extension grâce a son abréviation ou faites 'Fin' pour revenir au Menu.\n")
            elif choixextension == "Fin" :
                x = 1
            else :
                print("\nCe que vous avez saisi ne correspond a rien. Saisissez l'abréviation d'une extension pour l'ajouter et 'Fin' si vous voulez revenir au Menu.\n")
        x = 0

    #- Menu de suppression d'extension au dictionnaire -#
        
    if menu == "4" :
        print("\nLe dico contient les extensions suivantes :\n")
        tabcode = []
        for carte in list(dico):
            if dico[carte]["Extension"] not in tabcode :
                tabcode.append(str(dico[carte]["Extension"]))
        for i in tabcode :
            print(i)
        print("\nSelectionnez l'extension (en notant son abréviation) que vous voulez supprimer du dico ou ecrivez 'Fin' pour revenir au Menu.\n")
        while x != 1 :
            choixextension = input()
            if choixextension in tabcode :
                newextdico = '../extensions/'+choixextension+'.json'
                BD.removeset(newextdico,dico)
                fichier = open("BD/dico.txt","r")
                fichierdico = fichier.read()
                dico = eval(fichierdico)
                fichier.close()
                tabcode.remove(choixextension)
                print("\nL'extension a bien été supprimée.\nSupprimez une autre extension grâce a son abréviation ou faites 'Fin' pour revenir au Menu.\n")
            elif choixextension == "Fin" :
                x = 1
            else :
                print("\nCe que vous avez saisi ne correspond a rien. Saisissez l'abréviation d'une extension pour la supprimer et 'Fin' si vous voulez revenir au Menu.\n")
        x = 0

    #- Menu pour quitter le programme -#
        
    if menu == "5" :
        x = 1
#------------------------------------------------------------#
#Ouverture et fermeture du dictionnaire
dico = {}
fichier = open("BD/dico.txt","r")
fichierdico = fichier.read()
dico = eval(fichierdico)
fichier.close()


#c1 = set(dico["All That Glitters"]["Original Text"])
#c2 = set(dico["Archon of Absolution"]["Original Text"])
#print(WordMatrix.compare(c1, c2))
#BD.create_dico('../extensions/VOW.json',dico)
#BD.addset('../extensions/THB.json',dico)
#BD.removeset(  "ELD",dico)
#print(dico)

#-----------------------------------------------------------
#TEST DES CLUSTERS
"""matrice_test = {'Azerty':{'Bzerty':0.23,'Czerty':0.22,'Dzerty':0.90},'Bzerty':{'Azerty':0.23,'Czerty':0.96,'Dzerty':0.967},'Czerty':{'Azerty':0.22,'Bzerty':0.96,'Dzerty':0.267},'Dzerty':{'Azerty':0.90,'Bzerty':0.967,'Czerty':0.267}}
test = WordMatrix.WMatrix(dico)
cluster = WMCluster.coef_silhouette(matrice_test,3)
print(cluster)"""
#-----------------------------------------------------------

#Permet l'affichage du graphique d'illustration
""" 
test=WordMatrix.WMatrixGraph(dico) #Applique la fonction matrice pour la graphique

    #xy=[]
x=[] # Liste correspondant au nombre total de mots
y=[] # Liste correspondant au nombre de mots en commun
#Boucle permettant de remplir les listes
for i in range(0,len(test)-1):
    if test[i][1]>2:
        x.append(test[i][0])
        y.append(test[i][1])
        #xy.append((test[i][0],test[i][1]))
#---------------------------------------#

#Fonctions matplotlib permettant l'affichage du graphique
plt.scatter(x,y)
plt.xlabel("Total de mots")
plt.ylabel("Mots en commun")
plt.title("Illustration de la matrice")
plt.show()
#-------------------------------------------------------#
"""

"""fichier = open("matrice/clust.txt","w")
fichier.write(str(test))"""
#---------------------------------------------#

#Permet le test de similarité entre une carte choisie et les autres cartes

#test2=WordMatrix.WMatrix(dico)

"""on="oui"
while (on=="oui"): #Maintient le test pour ne pas avoir à relancer le programme à chaque fois
    print("Cartes présentes dans l'extension : \n") #Montre les cartes de l'extension
    for key in test2:
        print (key,"\n")
    
    choix= str(input("Ecrivez ici le nom de la carte dont vous voulez voir les scores de comparaisons \n")) #Permet la saisie de la carte à comparer
  
    for key in test2[choix]:
        if test2[choix][key]<0.9:
            print (key,":",test2[choix][key],"\n")
    on=str(input("Voulez-vous comparer une autre carte ? (oui/non) \n")) #Propose de comparer une autre carte


fichier = open("matrice/choix.txt","w")
fichier.write(str(test2))"""
#-------------------------------------------------------------------------#



