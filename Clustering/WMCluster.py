#coding:utf8
import numpy as np
import random

def Ajout_cartes_selon_leurs_distances(matrice, clusters, mcentre): #Ajout des cartes selon leurs distances de chaque centre
    clust={} #Initialisation de clust
    for centre in clusters:#Reset la liste pour ne pas que ca s'empile avec l'ancienne
        clust.setdefault(centre,[])
    for carte in matrice:#Pour chaque carte
        compteur_proche = 1#Initialisation du compteur de comparaison
        centre_proche = None#Initialisation du centre le plus proche
        for centre in mcentre:#Pour touts les clusters
            if(mcentre[centre]!=carte):
                if(matrice[carte][mcentre[centre]]<= compteur_proche):#On regarde si il est plus proche de la carte
                    centre_proche = centre
                    compteur_proche = matrice[carte][mcentre[centre]]#Valeur de la distance entre la carte et le centre
        clust[centre_proche].append(carte)#On ajoute la carte au cluster le plus proche
    return clust


def Comparaison_et_recentrage(matrice, clusters, mcentre, compteur):
    for centre in mcentre:
        somme_centre=0
        vrai_centre = mcentre[centre]
        for carte in clusters[centre]:#Boucle pour le centre
            if(carte!=mcentre[centre]):
                somme_centre+= matrice[mcentre[centre]][carte]#On fait la somme avec les distances du centre actuel

        for carte in clusters[centre]:#Boucle pour les cartes
            if(carte!=mcentre[centre]): #Bloquer la comparaison d'une carte avec elle-même
                somme_carte = matrice[carte][mcentre[centre]]
                for carte2 in clusters[centre]:
                    if(carte != carte2):
                        somme_carte += matrice[carte][carte2]
                
                if(somme_carte<somme_centre):
                    vrai_centre = carte
                    somme_centre = somme_carte
            if(vrai_centre != mcentre[centre]):
                clusters[centre].remove(vrai_centre) #On retire le nouveau centre de la liste
                clusters[centre].append(mcentre[centre]) #On ajoute l'ancien centre à la liste
                mcentre[centre]=vrai_centre #Le centre devient le nouveau centre
                compteur+=1#Mettre un compteur +1 comme ca si cela change, on sait si on doit relancer le programme ou non
    if(compteur==0):
        return compteur
    else:
        return [clusters, mcentre]
    

def cluster(matrice, n):
    liste_carte = list(matrice.keys())#Liste des noms de cartes
    centre_init={}#Dictionnaire des centres
    clusters_init = {}#Dictionnaire des clusters

    #Initialisation des différents centres
    c=random.sample(liste_carte,n)#Pour choisir les centres au hasard
    compt=0
    for j in c:
        clusters_init.setdefault(compt,[])
        centre_init[compt]=j
        compt+=1
    clusters = Ajout_cartes_selon_leurs_distances(matrice,clusters_init,centre_init)#Ajout des cartes dans leurs cluster proches
    compteur=0
    nouveau_cluster = Comparaison_et_recentrage(matrice,clusters, centre_init, compteur)

    if(nouveau_cluster==0):
        return clusters,centre_init

    else:
        while(nouveau_cluster!=0):#Si il y a eu des changements
            compteur=0
            mcluster=nouveau_cluster[0]#On récupère le cluster
            mcentre=nouveau_cluster[1]#On récupère le centre          
            clusters = Ajout_cartes_selon_leurs_distances(matrice, mcluster, mcentre)#Alors on refait les clusters
            nouveau_cluster=Comparaison_et_recentrage(matrice, mcluster, mcentre, compteur)#On refait les centres pour voir si cela change
        return clusters,mcentre #Si cela ne change pas c'est que les clusters sont bon alors on retur


def silhouette(matrice,n):
    score = [] #Récuperation de tout les scores silhouette (liste de dico)
    clusters= []#Récuperation de toutes les matrices (liste de dico)
    centres = []#Récupération de touts les centres (liste de dico)
    d_intra = []
    d_inter = []
    for i in range (4,n):
        matric = cluster(matrice,i)
        clusters.append(matric[0])
        centres.append(matric[1])
        compteur=0
    for i in range(0,len(centres)-1):#Chaque dico de centre (chaque tour de boucle de 2 a n centre)

        #Initialisation du rangement des résultats
        dico_score={}
        score.append(dico_score)#Permet de ranger lisiblement les résultats
        dico_intra={}
        d_intra.append(dico_intra)#Permet de ranger lisiblement les résultats
        dico_inter={}
        d_inter.append(dico_inter)#Permet de ranger lisiblement les résultats
        
        for j in centres[i]:#index de chaque centre dans chaque dico
            
            #Calcul intra cluster
            somme = 0
            for carte in clusters[i][j]:#Recupere les cartes contenu dans le cluster
                if carte != centres[i][j]:#Pour ne pas comparer les memes
                    somme += matrice[centres[i][j]][carte]#Ajout de la distance entre les 2 cartes
                    
            d_intra[i][j]= somme/len(centres[i][j]) #Recupere la somme des distances et on la divise par le nombre de carte du cluster

            #Calcul inter cluster
            somme_cluster = 0
            for dico in centres[i]:#Chaque dico de centre
                if centres[i][dico] != centres[i][j]:
                    somme_cluster += matrice[centres[i][j]][centres[i][dico]]#Ajout de la distance entre les 2 centres
            
            d_inter[i][j]=somme_cluster/len(centres[i]) #Recupere la somme des distances entre les centres diviser par le nombre de centre      
            #Score silhouette
            
            score[i][j]=(d_inter[i][j]-d_intra[i][j])/max(d_inter[i][j],d_intra[i][j])#Calcul de score de la silhouette
    return score,clusters       


def max_silhouette(matrice,budget,n):
    score = []#liste des scores max
    clusters = []#liste des clusters
    max_score = 0
    clusters_max = {}
    for i in range (1,budget):#Le nombre de budget
        score_silhouette = silhouette(matrice,n)#Lance les matrices budget fois
        for dico in score_silhouette[0]:#Dictionnaire de score max
            maxi=(-1)#Maximum
            for key in dico:#Chaque score
                if(maxi<dico[key]):#Pour récuperer le max pour chaque dico
                    maxi=dico[key]
            score.append(maxi)#Ajoute le score max
            
        for dico2 in score_silhouette[1]:#Pour chaque clusters
            clusters.append(dico2)#Ajoute a clusters
        
    max_score=max(score)#Donne le score max de tout les scores max
    clusters_max = clusters[score.index(max_score)]#Renvoie le clusters du score max
    return max_score,clusters_max
    
