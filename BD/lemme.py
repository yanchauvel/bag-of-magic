#coding:utf-8
import nltk
from nltk.stem import WordNetLemmatizer
from nltk.corpus import stopwords

#Récupération des "stopwords" (Mots récurents dont l'indexation est inutile)
stopwords = "../ressources/stopwords.txt"


#--- Fonction permettant la séparation du texte ---#

def tokenizer_fr():
    exceptions = "M\.|aujourd'hui|prud'hom\w+|pommes? de terre"
    nombres = '\d+\.\d+'
    mots = "\w+'?"
    ponctuations = """\.{3}|[+\.,;:?/!-"]"""
    re_mots = '|'.join([exceptions, nombres, mots, ponctuations])
    return nltk.RegexpTokenizer(re_mots)


#--- Fonction de lecture des stopwords ---#

def load_stopwords():
    with open(stopwords, 'r', encoding="utf-8") as f:
        lignes = f.readlines()
        res = []
        for ligne in lignes:
            res.extend([mot.strip() for mot in ligne.split(',')])
        return res


#--- Fonction de retrait des stopwords ---#

def remove_stopwords(tokens):
    return [tok for tok in tokens if len(tok.lower())>1 if (tok.lower() not in load_stopwords())]


#--- Fonction d'application de la tokensation et du retrait des stopwords ---#

def token(texte):
    texte = texte.read()
    texte = tokenizer_fr().tokenize(texte)
    texte = remove_stopwords(texte)
    return texte


#--- Fonction de lemmatisation des tokens du texte ---#

def tokenlem(text):
    text = tokenizer_fr().tokenize(text)
    x = 0
    for mot in text :
        if mot == "+":
            if x+1 < len(text) :
                if text[x+1] == "*" or "1" or "2" or "3" or "4" or "5":
                    if x+2 < len(text) :
                        if text[x+2] == "/":
                            text[x:x+5] = [''.join(text[x:x+5])]
                            x = x+1
                        else :
                            text[x:x+1] = [''.join(text[x:x+1])]
                            x = x+1
                    else :
                        x = x+1
        if mot == "*" or "1" or "2" or "3" or "4" or "5":
            if x+1 < len(text) :
                if text[x+1] == "/":
                    if x+2 < len(text) :
                        text[x:x+3] = [''.join(text[x:x+3])]
                        x = x+1
                else :
                    x = x+1
        else :
            x = x+1
    text = remove_stopwords(text)
    return text


#--- Fonction de lemmatisation des tokens du texte ---#

def lemme(token):
    lemma = WordNetLemmatizer()
    dico = []
    for mot in token :
        lemmat = lemma.lemmatize(mot)
        dico.append(lemmat)
    return dico

