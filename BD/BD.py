# coding:utf8
import json
import nltk
from BD import lemme
from nltk.corpus import stopwords

#Récupération des "stopwords" (Mots récurents dont l'indexation est inutile)
stopwords = stopwords.words('english')


#--- Fonction de création du dictionnaire de cartes ---#

def create_dico(file,dico):
    with open(file, 'r', encoding='UTF-8') as f:
        dico = {}
        extension = json.load(f)
        #récupération du code de l'extension
        code = extension["data"]["code"]
        #parcours et récupération du contenu de l'extension
        for x in extension["data"]["cards"]:
            if "originalText" in dict.keys(x):
                #lemmatisation du "Original Text" de la carte
                lem = lemme.lemme(lemme.tokenlem(x["originalText"]))
                if len(lem) > 2 : 
                    dico[x["name"]]={}
                    if len(x["colorIdentity"])== 0:
                        dico[x["name"]]["Color Identity"]=None
                    else :
                        dico[x["name"]]["Color Identity"]=x["colorIdentity"]
                    if len(x["colorIdentity"])== 0:
                        dico[x["name"]]["Colors"]=None
                    else :
                        dico[x["name"]]["Colors"]=x["colors"]
                    if "convertedManaCost" in dict.keys(x):
                        dico[x["name"]]["Converted mana cost"]=x["convertedManaCost"]
                    if "keywords" in dict.keys(x):
                        dico[x["name"]]["Keywords"]=x["keywords"]
                    if "manaCost" in dict.keys(x):
                        dico[x["name"]]["Mana Cost"]=x["manaCost"]
                    if "manaValue" in dict.keys(x):
                        dico[x["name"]]["Mana Value"]=x["manaValue"]
                    if "type" in dict.keys(x):
                        dico[x["name"]]["Type"]=x["type"]
                    dico[x["name"]]["originalText"]=lem
                    #Création de triplet de mots
                    ngramme = []
                    for i in range(0,len(lem)-2):
                        ngramme.append(''.join((lem[i],lem[i+1],lem[i+2])))
                    dico[x["name"]]["n-gramme"]=ngramme
                    if "power" in dict.keys(x):
                        dico[x["name"]]["Power"]=x["power"]
                    if "toughness" in dict.keys(x):
                        dico[x["name"]]["Toughness"]=x["toughness"]
                    dico[x["name"]]["Extension"]= code
    #écriture dans le fichier contenant le dictionnaire de cartes            
    fichier = open("BD/dico.txt","w",encoding="UTF-8")
    strdico = str(dico)
    fichier.write(strdico)           
    return dico


#--- Fonction d'ajout d'extension (Similaire a "create_dico") ---#

def addset(file,dico):
    with open(file, 'r', encoding='UTF-8') as f:
        extension = json.load(f)
        code = extension["data"]["code"]
        for x in extension["data"]["cards"]:
             if "originalText" in dict.keys(x) :
                lem = lemme.lemme(lemme.tokenlem(x["originalText"]))
                if len(lem) > 2 :
                    dico[x["name"]]={}
                    if len(x["colorIdentity"])== 0:
                        dico[x["name"]]["Color Identity"]=None
                    else :
                        dico[x["name"]]["Color Identity"]=x["colorIdentity"]
                    if len(x["colorIdentity"])== 0:
                        dico[x["name"]]["Colors"]=None
                    else :
                        dico[x["name"]]["Colors"]=x["colors"]
                    if "convertedManaCost" in dict.keys(x):
                        dico[x["name"]]["Converted mana cost"]=x["convertedManaCost"]
                    if "keywords" in dict.keys(x):
                        dico[x["name"]]["Keywords"]=x["keywords"]
                    if "manaCost" in dict.keys(x):
                        dico[x["name"]]["Mana Cost"]=x["manaCost"]
                    if "manaValue" in dict.keys(x):
                        dico[x["name"]]["Mana Value"]=x["manaValue"]
                    if "type" in dict.keys(x):
                        dico[x["name"]]["Type"]=x["type"]
                    lem = lemme.lemme(lemme.tokenlem(x["originalText"]))
                    ngramme = []
                    for i in range(0,len(lem)-2):
                        ngramme.append(''.join((lem[i],lem[i+1],lem[i+2])))
                    dico[x["name"]]["n-gramme"]=ngramme  
                    if "power" in dict.keys(x):
                        dico[x["name"]]["Power"]=x["power"]
                    if "toughness" in dict.keys(x):
                        dico[x["name"]]["Toughness"]=x["toughness"]
                    dico[x["name"]]["Extension"]= code
    fichier = open("BD/dico.txt","w")
    strdico = str(dico)
    fichier.write(strdico)       
    return dico


#--- Fonction de retrait d'extension ---#

def removeset(file,dico):
    #Parcours les cartes du dictionnaire et retire celle ayant un code similaire a file
    with open(file, 'r', encoding='UTF-8') as f:
        extension = json.load(f)
        code = extension["data"]["code"]
        for carte in list(dico):
            for key,value in dico[carte].items():
                if value == code :
                    dico.pop(carte)
        fichier = open("BD/dico.txt","w")
        strdico = str(dico)
        fichier.write(strdico)            
        return dico

