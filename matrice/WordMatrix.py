# coding:utf8
from matrice import Matrix
import nltk
#Matrice du texte des cartes
dico={}
fichier = open("BD/dico.txt","r")#Ouverture du fichier qui contient la liste des cartes
fichierdico = fichier.read()#Lecture du fichier
dico = eval(fichierdico)#Permet de récupérer les données sous forme de dictionnaire
fichier.close()


def graph (c1, c2):#Renvoie les coordonées de Jaccard entre deux cartes
    x=len(c1.union(c2))
    y=len(c1.intersection(c2))
    return x,y


    
def compare(c1, c2):#Renvoie le résultat de la distance de Jackar entre deux cartes
    jacc= 1-(len(c1.intersection(c2))/len(c1.union(c2)))
    return jacc

def WMatrixGraph(dico):#Renvoie un tableau de coordonnées 
    dist=[]
    for i in dico :
        c1=set(dico[i]["n-gramme"])
        for j in dico :
            if (j != i) :
                c2= set(dico[j]["n-gramme"])
                comp=graph(c1,c2)
                dist.append(comp)
    return dist

def WMatrix(dico):#Renvoie un dictionaire pour chaque carte sa comparaison aux autres
    xmat={}
    for i in dico :
        c1=set(dico[i]["n-gramme"])
        xmat.setdefault(i,{})#Initialisation de la clef
        for j in dico :
            if (j != i) :
                c2= set(dico[j]["n-gramme"])
                comp=compare(c1,c2)
                result={}
                result[j]=comp
                xmat[i].update(result)
    return xmat
